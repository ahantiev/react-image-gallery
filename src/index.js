import React from 'react'
import { render } from 'react-dom'
import { applyMiddleware, createStore } from 'redux'
import { Provider } from 'react-redux'
import promiseMiddleware from 'redux-promise-middleware';
import thunkMiddleware from 'redux-thunk';
import reducer from './reducers'
import App from './components/App'

const store = createStore(reducer, {}, applyMiddleware(
    thunkMiddleware,
    promiseMiddleware()
));

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
