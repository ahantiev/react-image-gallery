import React, {Component, PropTypes} from "react";
import {connect} from "react-redux";
import {Button} from "react-bootstrap";
import FavoritesBox from "../containers/FavoritesBox";
import {toggleFavoriteSidebar} from "../actions/index";

class FavoritesSidebar extends Component {
    static propTypes = {
        favoritesCount: PropTypes.number.isRequired,
        isOpen: PropTypes.bool.isRequired,
    };

    toggleSidebar = () => {
        this.props.dispatch(toggleFavoriteSidebar());
    };

    getSidebar = () => {
        return <div className="toggle-favorite-box-container">
            <Button bsStyle="link"
                    className="close-sidebar-btn"
                    onClick={this.toggleSidebar}
            >
                <span className="glyphicon glyphicon-remove"></span>
            </Button>
            <FavoritesBox columns={2}/>

        </div>
    };

    getButton = () => {
        return <div className="toggle-favorite-box-container-button">
            <Button bsSize="large"
                    bsStyle="link"
                    onClick={this.toggleSidebar}
                    >
                <span className="glyphicon glyphicon-heart"></span> {this.props.favoritesCount}
            </Button>
        </div>;
    };

    render = () => {
        if(this.props.isOpen) {
            return this.getSidebar();
        } else {
            return this.getButton();
        }
    }
}
const mapStateToProps = (state) => ({
    isOpen: state.sidebar.isOpen,
    favoritesCount: state.favorites.length,
});

export default connect(
    mapStateToProps
)(FavoritesSidebar)

