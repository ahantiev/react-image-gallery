import React, {Component, PropTypes} from "react";
import {connect} from "react-redux";
import {Row, Col} from "react-bootstrap";
import Masonry from "react-masonry-component";
import {deleteFavorite} from "../actions/index";
import FavoriteBoxItem from "../components/FavoriteBoxItem";

const masonryOptions = {
    transitionDuration: 0
};

class FavoriteBox extends Component {
    static propTypes = {
        favorites: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.number.isRequired,
            url: PropTypes.string.isRequired,
        }).isRequired).isRequired,
        columns: PropTypes.number.isRequired,
    };

    deleteFromFavorite = (photoId) => {
        this.props.dispatch(deleteFavorite(photoId));
    };

    getItems = () => {
        const photos = this.props.favorites;
        const size = 12 / this.props.columns;
        return photos.map((photo) => {
            return (
                <Col key={photo.id} lg={size} md={size} sm={size} xs={size} className="search-box-item">
                    <FavoriteBoxItem  deleteFromFavorite={this.deleteFromFavorite} {...photo}/>
                </Col>
            );
        });
    };

    getContentNotFound = () => {
        return <div className="content-not-found-box">
            <h2>No favorites</h2>
            <h3>Click on <span className="glyphicon glyphicon-heart color-red"></span> to add to favorites</h3>
        </div>
    };

    render = () => {
        if(this.props.favorites.length === 0) {
            return this.getContentNotFound();
        }

        return (
            <div>
                <h3>Favorites ({this.props.favorites.length})</h3>
                <Row className="favorite-box-container">
                    <Masonry elementType={'div'} options={masonryOptions}>
                        {this.getItems()}
                    </Masonry>
                </Row>
            </div>
        )
    }
}
const mapStateToProps = (state) => ({
    favorites: state.favorites,
});

export default connect(
    mapStateToProps
)(FavoriteBox)

