import * as _ from "lodash";
import React, {Component, PropTypes} from "react";
import {connect} from "react-redux";
import {Row} from "react-bootstrap";
import Masonry from "react-masonry-component";
import {addFavorite, deleteFavorite} from "../actions/index";
import SearchBoxItem from "../components/SearchBoxItem";

const masonryOptions = {
    transitionDuration: 0
};

class SearchResultBox extends Component {
    static propTypes = {
        photos: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.number.isRequired,
            url: PropTypes.string.isRequired,
        }).isRequired).isRequired,
        favorites: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.number.isRequired,
            url: PropTypes.string.isRequired,
        }).isRequired).isRequired,
        isFetching: PropTypes.bool.isRequired

    };

    addToFavorites = (photo) => {
        this.props.dispatch(addFavorite(photo));
    };

    deleteFromFavorites = (photo) => {
        this.props.dispatch(deleteFavorite(photo));
    };

    isFavorite(photo) {
        const result = _.find(this.props.favorites, {id: photo.id});
        return result != null;
    }

    getItems = () => {
        const photos = this.props.photos;
        return photos.map((photo) => {
            return (
                <SearchBoxItem key={photo.id}
                               isFavorite={this.isFavorite(photo)}
                               addToFavorites={this.addToFavorites}
                               deleteFromFavorites={this.deleteFromFavorites}
                               {...photo}
                />
            );
        });
    };

    getContentNotFound = () => {
        return <div className="content-not-found-box">
            <h2>No photos found.</h2>
            <h3>Try to use the search</h3>
        </div>
    };

    getLoader = () => {
        return <div className="content-not-found-box">
            <h2>Loading...</h2>
        </div>
    };

    render = () => {
        if(this.props.isFetching) {
            return this.getLoader();
        }

        if(this.props.photos.length === 0) {
            return this.getContentNotFound();
        }

        return (
            <div>
                <h3>Results</h3>
                <Row className="search-box-container">
                    <Masonry elementType={'div'} options={masonryOptions}>
                        {this.getItems()}
                    </Masonry>
                </Row>
            </div>
        )
    }
}
const mapStateToProps = (state) => ({
    photos: state.search.photos,
    favorites: state.favorites,
    isFetching: state.search.isFetching,
});

export default connect(
    mapStateToProps
)(SearchResultBox)
