import React, {Component} from "react";
import {connect} from "react-redux";
import {Button, InputGroup, Form, FormGroup, FormControl} from "react-bootstrap";
import {searchPhotos} from "../actions/index";

class SearchForm extends Component {
    state = {
        text: ''
    };

    handleChange = (e) => {
        this.setState({text: e.target.value});
    }

    onSubmit = (e) => {
        e.preventDefault();
        let text = this.state.text;
        if (!text.trim()) {
            return
        }
        this.props.dispatch(searchPhotos(text));
    };

    render = () => {
        return (
            <div className="search-form-container-fluid">
                <Form onSubmit={this.onSubmit} className="container search-form-container">
                    <FormGroup bsSize="large">
                        <InputGroup>
                            <FormControl type="text" value={this.state.text} onChange={this.handleChange}/>
                            <InputGroup.Button>
                                <Button bsSize="large" bsStyle="primary" type="submit">Search</Button>
                            </InputGroup.Button>
                        </InputGroup>
                    </FormGroup>
                </Form>
            </div>
        )
    }
}

export default connect()(SearchForm)

