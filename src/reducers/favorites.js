import * as ActionTypes from '../constants/ActionTypes'

const favorites = (state = [], action) => {
    switch (action.type) {
        case ActionTypes.ADD_FAVORITE:
            return [
                ...state,
                action.photo
            ];
        case ActionTypes.DELETE_FAVORITE:
            return state.filter(favorite => favorite.id !== action.id);
        default:
            return state
    }
}

export default favorites;