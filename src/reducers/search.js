import * as ActionTypes from '../constants/ActionTypes'

var initialState = {
    isFetching: false,
    photos: []
};

const search = (state = initialState, action) => {
    switch (action.type) {
        case `${ActionTypes.RECEIVE_PHOTOS}_FULFILLED`:
            return {
                ...state,
                isFetching: false,
                photos: createPhotos(action.payload)
            };
        case `${ActionTypes.RECEIVE_PHOTOS}_PENDING`:
            return {
                ...state,
                isFetching: true,
                photos: []
            };
        case `${ActionTypes.RECEIVE_PHOTOS}_REJECTED`:
            return {
                ...state,
                isFetching: false,
                photos: []
            };
        default:
            return state
    }
};

function createPhotos(payload) {
    return payload.map(function (photo) {
        return {
            id: photo.id,
            url: photo.photo_604
        }
    });
}

export default search;