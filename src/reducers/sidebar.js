import * as ActionTypes from '../constants/ActionTypes'

const sidebar = (state = {isOpen: false}, action) => {
    switch (action.type) {
        case ActionTypes.TOGGLE_FAVORITE_SIDEBAR:
            return {
                ...state,
                isOpen: !state.isOpen
            };
        default:
            return state
    }
}

export default sidebar;