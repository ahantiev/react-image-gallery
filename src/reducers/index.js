import { combineReducers } from 'redux';
import search from './search'
import favorites from './favorites'
import sidebar from './sidebar'

const todoApp = combineReducers({
  search,
  favorites,
  sidebar
});

export default todoApp
