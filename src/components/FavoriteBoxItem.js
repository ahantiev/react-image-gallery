import React, {Component, PropTypes} from "react";
import {Button, OverlayTrigger, Tooltip} from "react-bootstrap";
import Photo from "./Photo";

class FavoriteBoxItem extends Component {
    static propTypes = {
        id: PropTypes.number.isRequired,
        url: PropTypes.string.isRequired,
        deleteFromFavorite: PropTypes.func.isRequired,
    };

    tooltip = (
        <Tooltip id="tooltip"><strong>Delete from Favorites!</strong></Tooltip>
    );

    render = () => {
        return (
            <div>
                <Photo id={this.props.id} url={this.props.url}/>
                <OverlayTrigger placement="top" overlay={this.tooltip}>
                    <Button bsSize="xsmall"
                            bsStyle="link"
                            className="action-btn delete-action-btn"
                            onClick={() => this.props.deleteFromFavorite(this.props.id)}>
                        <span className="glyphicon glyphicon-remove"></span>
                    </Button>
                </OverlayTrigger>
            </div>
        )
    }
}
export default FavoriteBoxItem;

