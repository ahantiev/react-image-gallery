import React from "react";
import SearchForm from "../containers/SearchForm";
import PhotoList from "../containers/SearchBox";
import FavoritesBox from "../containers/FavoritesBox";
import ToggleFavoriteBox from "../containers/FavoritesSidebar";
import MediaQuery from "react-responsive";
import {Grid, Row, Col} from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/css/bootstrap-theme.css";
import "../../public/style/style.css";

const App = (favorites) => (
    <div className="app">
        <MediaQuery query='(max-width: 1000px)'>
            <ToggleFavoriteBox/>
        </MediaQuery>
        <div className="app-container">
            <SearchForm/>
            <Grid>
                <Row>
                    <Col xs={12} md={8}><PhotoList/></Col>
                    <Col xs={6} md={4}>
                        <MediaQuery query='(min-width: 1000px)'>
                            <FavoritesBox columns={2}/>
                        </MediaQuery>
                    </Col>
                </Row>
            </Grid>
        </div>
    </div>
);

export default App;
