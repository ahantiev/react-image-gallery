import React, {Component, PropTypes} from "react";
import {Image} from "react-bootstrap";

class Photo extends Component {
    static propTypes = {
        id: PropTypes.number.isRequired,
        url: PropTypes.string.isRequired,
    };

    render = () => {
        return (
            <Image src={this.props.url} responsive className="thumbnail"/>
        )
    }
}
export default Photo;

