import React, {Component, PropTypes} from "react";
import {Button, Col, Tooltip, OverlayTrigger} from "react-bootstrap";
import Photo from "./Photo";

class SearchBoxItem extends Component {
    static propTypes = {
        id: PropTypes.number.isRequired,
        url: PropTypes.string.isRequired,
        isFavorite: PropTypes.bool.isRequired,
        addToFavorites: PropTypes.func.isRequired,
        deleteFromFavorites: PropTypes.func.isRequired,
    };

    getTooltip = (tooltipText) => (
        <Tooltip id="tooltip"><strong>{tooltipText}</strong></Tooltip>
    );

    onClickFavoriteBtn = () => {
        if(this.props.isFavorite) {
            this.props.deleteFromFavorites(this.props.id);
        } else {
            this.props.addToFavorites({id: this.props.id, url: this.props.url});
        }
    };

    render = () => {
        let favoriteBtnClasses = "action-btn favorite-action-btn";
        let tooltipText;
        if(this.props.isFavorite) {
            favoriteBtnClasses += ' active';
            tooltipText =  'Delete from Favorites'
        } else {
            tooltipText =  'Add to Favorites';
        }

        return (
            <Col lg={4} md={6} sm={6} xs={12} className="search-box-item">
                <Photo id={this.props.id} url={this.props.url}/>
                <OverlayTrigger placement="top" overlay={this.getTooltip(tooltipText)}>
                    <Button bsSize="xsmall"
                            bsStyle="link"
                            className={favoriteBtnClasses}
                            onClick={this.onClickFavoriteBtn}>
                        <span className="glyphicon glyphicon-heart"></span>
                    </Button>
                </OverlayTrigger>
            </Col>
        )
    }
}
export default SearchBoxItem;

