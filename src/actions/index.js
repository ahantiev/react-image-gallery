import vk from "../api/vk";
import * as ActionTypes from "../constants/ActionTypes";

export const searchPhotos = (query) => dispatch => {
    dispatch({
        type: ActionTypes.RECEIVE_PHOTOS,
        payload: vk.photosSearch(query)
    })
};

export const addFavorite = (photo) => dispatch => {
    dispatch({
        type: ActionTypes.ADD_FAVORITE,
        photo: photo
    })
};


export const deleteFavorite = (photoId) => dispatch => {
    dispatch({
        type: ActionTypes.DELETE_FAVORITE,
        id: photoId
    })
};

export const toggleFavoriteSidebar = () => dispatch => {
    dispatch({
        type: ActionTypes.TOGGLE_FAVORITE_SIDEBAR,
    })
};


