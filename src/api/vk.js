import jsonp from "jsonp";

const VK_API = 'https://api.vk.com/method';

export default {
    photosSearch: (query) => {
        return new Promise((resolve, reject) => {
            jsonp(`${VK_API}/photos.search?q=${query}&v=5.59&count=100`, {}, function (err, res) {
                        if (err) {
                            return reject(err);
                        }
                        resolve(res.response.items);
                    });
            });
    }
}
